# Common PySC2 libraries

This page lists some common libraries and their classes/methods to supplement [readme](https://github.com/deepmind/pysc2/blob/master/README.md) and [docs](https://github.com/deepmind/pysc2/tree/master/docs) in the pysc2 repo. This page is updated for PySC2 v2.0.1 

Listings below use the following conventions:

> Library name (link to code)
> - Class
>   - Method

## Contents
1. [base_agent](#base_agent)
2. [random_agent](#random_agent)
3. [scripted_agent](#scripted_agent)
4. [sc2_env](#sc2_env)
5. [actions](#actions)
6. [features](#features)
7. [units](#units)


## Libraries

### base_agent

**from pysc2.agents import base_agent** ([code](https://github.com/deepmind/pysc2/blob/master/pysc2/agents/base_agent.py))
- BaseAgent(object)
  - setup(self, obs_spec, action_spec)
  - reset(self)
  - step(self, obs)

### random_agent

**from pysc2.agents import random_agent** ([code](https://github.com/deepmind/pysc2/blob/master/pysc2/agents/random_agent.py))
- RandomAgent(base_agent.BaseAgent)
  - step(self, obs)

### scripted_agent

**from pysc2.agents import scripted_agent** ([code](https://github.com/deepmind/pysc2/blob/master/pysc2/agents/scripted_agent.py))
- MoveToBeacon(base_agent.BaseAgent)
  - step(self, obs)
- CollectMineralShards(base_agent.BaseAgent)
  - step(self, obs)
- CollectMineralShardsFeatureUnits(base_agent.BaseAgent)
  - setup(self, obs_spec, action_spec)
  - reset(self)
  - step(self, obs)
- DefeatRoaches(base_agent.BaseAgent)
  - step(self, obs)

### sc2_env

**from pysc2.env import sc2_env** ([code](https://github.com/deepmind/pysc2/blob/master/pysc2/env/sc2_env.py))
- Race(enum.IntEnum)
- Difficulty(enum.IntEnum)
- SC2Env(environment.Base)
  - observation_spec(self)
  - action_spec(self)
  - reset(self)
  - step(self, actions)
  - send_chat_messages(self, messages)
  - save_replay(self, replay_dir, prefix=None)
  - close(self)

### actions

**from pysc2.lib import actions** ([code](https://github.com/deepmind/pysc2/blob/master/pysc2/lib/actions.py))
- ActionSpace(enum.Enum)
  - spatial(action, action_space)
  - no_op(action, action_space)
  - move_camera(action, action_space, minimap)
  - select_point(action, action_space, select_point_act, screen)
  - select_rect(action, action_space, select_add, screen, screen2)
  - select_idle_worker(action, action_space, select_worker)
  - select_army(action, action_space, select_add)
  - select_warp_gates(action, action_space, select_add)
  - select_larva(action, action_space)
  - select_unit(action, action_space, select_unit_act, select_unit_id)
  - control_group(action, action_space, control_group_act, control_group_id)
  - unload(action, action_space, unload_id)
  - build_queue(action, action_space, build_queue_id)
  - cmd_quick(action, action_space, ability_id, queued)
  - cmd_screen(action, action_space, ability_id, queued, screen)
  - cmd_minimap(action, action_space, ability_id, queued, minimap)
  - autocast(action, action_space, ability_id)
- ArgumentType(collections.namedtuple(
  - enum(cls, options, values)
  - factory(i, name)
  - scalar(cls, value)
  - point(cls)
  - factory(i, name)
  - spec(cls, id_, name, sizes)
- Arguments(collections.namedtuple("Arguments", [
  - types(cls, **kwargs)
- Function(collections.namedtuple(
  - ui_func(cls, id_, name, function_type, avail_fn=always)
  - ability(cls, id_, name, function_type, ability_id, general_id=0)
  - spec(cls, id_, name, args)
  - str(self, space=False)
- Functions(object)
- FunctionCall(collections.namedtuple(
  - init_with_validation(cls, function, arguments)
  - all_arguments(cls, function, arguments)
- ValidActions(collections.namedtuple(

### features

**from pysc2.lib import features** ([code](https://github.com/deepmind/pysc2/blob/master/pysc2/lib/features.py))
- FeatureType(enum.Enum)
- PlayerRelative(enum.IntEnum)
- Visibility(enum.IntEnum)
- Effects(enum.IntEnum)
- ScoreCumulative(enum.IntEnum)
- Player(enum.IntEnum)
- UnitLayer(enum.IntEnum)
- FeatureUnit(enum.IntEnum)
- Feature(collections.namedtuple(
  - unpack(self, obs)
  - unpack_layer(plane)
  - unpack_rgb_image(plane)
  - color(self, plane)
- ScreenFeatures(collections.namedtuple("ScreenFeatures", [
- MinimapFeatures(collections.namedtuple("MinimapFeatures", [
- Dimensions(object)
  - screen(self)
  - minimap(self)
- AgentInterfaceFormat(object)
  - feature_dimensions(self)
  - rgb_dimensions(self)
  - action_space(self)
  - camera_width_world_units(self)
  - use_feature_units(self)
  - hide_specific_actions(self)
  - action_dimensions(self)
  - parse_agent_interface_format(
  - features_from_game_info(
- Features(object)
  - observation_spec(self)
  - action_spec(self)
  - transform_obs(self, obs)
  - or_zeros(layer, size)
  - unit_vec(u)
  - feature_unit_vec(u)
  - available_actions(self, obs)
  - transform_action(self, obs, func_call, skip_available=False)
  - reverse_action(self, action)
  - func_call_ability(ability_id, cmd_type, *args)

### units

**from pysc2.lib import units** ([code](https://github.com/deepmind/pysc2/blob/master/pysc2/lib/units.py))
- Neutral(enum.IntEnum)
- Protoss(enum.IntEnum)
- Terran(enum.IntEnum)
- Zerg(enum.IntEnum)

