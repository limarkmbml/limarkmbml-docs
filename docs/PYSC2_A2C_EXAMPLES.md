# Overview
This list shows examples of A2C used with pysc2

The listings follow this convention:
> **Repository name (link to repo)**
>
> Filename (link to code)
> - Class
>   - Method

# Contents

|**EXAMPLES**|**A2C**|**FullyConv**|**Atarinet**|**Others**|**Feature vectors**|**Based** **on**|
|---|---------|--------|------|---------------|---------|---|
|1. [ConstantinosM-A2C-ConvLSTM-Starcraft2-with-pysc2](#ConstantinosM-A2C-ConvLSTM-Starcraft2-with-pysc2)|x|x||LSTM||pekaalto|
|2. [iceman126-pysc2-agents](#iceman126-pysc2-agents)|x|x|x||||
|3. [inoryy-pysc2-rl-agent](#inoryy-pysc2-rl-agent)|x|x||HRL, Aux Tasks|spatial and non-spatial|pekaalto, xhujoy, simonmeister (loosely)|
|4. [chris-chris-pysc2-examples](#chris-chris-pysc2-examples)|x|x||DQN - OpenAI baselines|||
|5. [simonmeister-pysc2-rl-agents](#simonmeister-pysc2-rl-agents)|x|x||||OpenAI baselines (adapted), pekaalto|
|6. [pekaalto-sc2aibot](#pekaalto-sc2aibot)|x|x||PPO|spatial|OpenAI baselines (adapted), xhujoy|


# List of examples

## ConstantinosM-A2C-ConvLSTM-Starcraft2-with-pysc2

**Repo: ConstantinosM/A2C-ConvLSTM-Starcraft2-with-pysc2** ([repo link](https://github.com/ConstantinosM/A2C-ConvLSTM-Starcraft2-with-pysc2))

File: ConstantinosM/A2C-ConvLSTM-Starcraft2-with-pysc2/actorcritic/runner.py ([code](https://github.com/ConstantinosM/A2C-ConvLSTM-Starcraft2-with-pysc2/blob/master/actorcritic/runner.py))
- Runner(object)
  - reset(self)
  - run_batch(self)
  - run_trained_batch(self)

File: ConstantinosM/A2C-ConvLSTM-Starcraft2-with-pysc2/common/multienv.py ([code](https://github.com/ConstantinosM/A2C-ConvLSTM-Starcraft2-with-pysc2/blob/master/common/multienv.py))
- SingleEnv
  - step(self, actions)
  - reset_done_envs(self)
  - reset(self)
  - close(self)
  - worker(remote, env_fn_wrapper)
- CloudpickleWrapper(object)
- SubprocVecEnv
  - step(self, actions)
  - reset(self)
  - close(self)
  - reset_done_envs(self)
  - make_sc2env(**kwargs)

File: ConstantinosM/A2C-ConvLSTM-Starcraft2-with-pysc2/common/preprocess.py ([code](https://github.com/ConstantinosM/A2C-ConvLSTM-Starcraft2-with-pysc2/blob/master/common/preprocess.py))
  - log_transform(x, scale)
  - get_visibility_flag(visibility_feature)
  - numeric_idx_and_scale(set)
  - stack_list_of_dicts(d)
  - get_available_actions_flags(obs)
- ObsProcesser
  - get_screen_numeric(self, obs)
  - get_mimimap_numeric(self, obs)
  - process_one_input(self, timestep
  - process(self, obs_list)
  - combine_batch(self, mb_obs)
  - make_default_args(arg_names)
  - convert_point_to_rectangle(point, delta, dim)
  - l(x)
  - arg_names()
  - find_rect_function_id()
- ActionProcesser
  - make_one_action(self, action_id, spatial_coordinates)
  - process(self, action_ids, spatial_action_2ds)
  - combine_batch(self, mb_actions)

File: ConstantinosM/A2C-ConvLSTM-Starcraft2-with-pysc2/common/test_processers.py ([code](https://github.com/ConstantinosM/A2C-ConvLSTM-Starcraft2-with-pysc2/blob/master/common/test_processers.py))
- TestActionProcesser(TestCase)
  - test_simple(self)
  - test_rectangle(self)
  - test_invalid_and_dim(self)
- TestObsProcesser
  - test_one_input(self)

File: ConstantinosM/A2C-ConvLSTM-Starcraft2-with-pysc2/common/test_util.py ([code](https://github.com/ConstantinosM/A2C-ConvLSTM-Starcraft2-with-pysc2/blob/master/common/test_util.py))
- TestUtil(tf.test.TestCase)
  - test_dict_list_transpose(self)
  - test_weighted_random_sample(self)
  - test_select_from_each_row(self)
  - test_calculate_n_step_reward(self)
  - discount_with_dones(rewards, dones, gamma)
  - test_calculate_n_step_reward1(self)
  - test_combine_first_dimensions(self)
  - test_ravel_index_pairs(self)
- TestGeneralNStepAdvantage(tf.test.TestCase)
  - test_gamma_one(self)
  - test_gamma_zero(self)
  - test_general(self)
  - test_general_batch(self)
  - test_general_n_step_advantage_3(self)

File: ConstantinosM/A2C-ConvLSTM-Starcraft2-with-pysc2/common/util.py ([code](https://github.com/ConstantinosM/A2C-ConvLSTM-Starcraft2-with-pysc2/blob/master/common/util.py))
  - weighted_random_sample(weights)
  - select_from_each_row(params, indices)
  - calculate_n_step_reward(
  - general_n_step_advantage(
  - combine_first_dimensions(x
  - ravel_index_pairs(idx_pairs, n_col)
  - dict_of_lists_to_list_of_dicst(x
- ActorCriticAgent
  - init(self)
  - build_model(self)
  - logclip(x)
  - step(self, obs, rnn_state)
  - train(self, input_dict, rnn_state)
  - get_value(self, obs, rnn_state)
  - flush_summaries(self)
  - save(self, path, step=None)
  - load(self, path)

File: ConstantinosM/A2C-ConvLSTM-Starcraft2-with-pysc2/run_agent.py ([code](https://github.com/ConstantinosM/A2C-ConvLSTM-Starcraft2-with-pysc2/blob/master/run_agent.py))
  - check_and_handle_existing_folder(f)
  - main()

## iceman126-pysc2-agents

**Repo: iceman126/pysc2-agents** ([repo link](https://github.com/iceman126/pysc2-agents))

File: iceman126/pysc2-agents/a2c.py ([code](https://github.com/iceman126/pysc2-agents/blob/master/a2c.py))
  - learn(network, log_path, env_function, num_cpu=4, nsteps=10, total_timesteps=int(80e6), optimizer="rmsprop", vl_coef=1.0, ent_coef=1e-3, max_grad_norm=0.5, ar=True, lr=7e-4, gamma=0.99)

File: iceman126/pysc2-agents/agent.py ([code](https://github.com/iceman126/pysc2-agents/blob/master/agent.py))
- Agent(object)
  - training_process(self, epoch, train_steps, epsilon)
  - evaluating_process(self, epoch, episodes)
  - step_policy_ar(self, screen, minimap, ns, *_args, **_kwargs)
  - step_policy(self, screen, minimap, ns, *_args, **_kwargs)
  - step_policy(self, screen, minimap, ns, *_args, **_kwargs)
  - step_epsilon(self, screen, minimap, ns, *_args, **_kwargs)
  - train(self, lr, screen, minimap, ns, states, rewards, masks, acts, use_spatial_actions, available_actions, pos, values, args, args_used)
  - value(self, screen, minimap, ns, *_args, **_kwargs)
  - save_model(self, epoch)
  - remake_env(self, num_cpu)

File: iceman126/pysc2-agents/common/vec_env/subproc_vec_env.py ([code](https://github.com/iceman126/pysc2-agents/blob/master/common/vec_env/subproc_vec_env.py))
  - worker(remote, parent_remote, env_fn_wrapper)
- CloudpickleWrapper(object)
- SubprocVecEnv(VecEnv)
  - step(self, actions, arg_dicts)
  - reset(self)
  - step_async(self)
  - step_wait(self)
  - save_replay(self, path, epoch)
  - reset_task(self)
  - close(self)
  - nenvs(self)
  - base_action_count(self)
  - construct_action(screen_space, act_id, selected_args)
  - step(action, arg_dict)
  - concat_obs(obs, infos)
  - process_state(state)
  - process_catagorical_map(input_map, scale)
  - save_replay(epoch)

File: iceman126/pysc2-agents/misc.py ([code](https://github.com/iceman126/pysc2-agents/blob/master/misc.py))
  - set_global_seeds(i)
  - is_spatial_action(act_ids)
  - make_path(f)

File: iceman126/pysc2-agents/model.py ([code](https://github.com/iceman126/pysc2-agents/blob/master/model.py))
- Model(object)
  - load(load_path)
  - print_params()

File: iceman126/pysc2-agents/networks.py ([code](https://github.com/iceman126/pysc2-agents/blob/master/networks.py))
  - AtariNet(scope, screen_space, minimap_space, ns_space, nact, ar=True)
  - fc_spatial_argument(state, height, width, name)
  - embed_features(t, input_features)
  - FullyConvNet(scope, screen_space, minimap_space, ns_space, nact)
  - spatial_argument(state, name)
  - non_spatial_argument(fc, size, name)

File: iceman126/pysc2-agents/policies.py ([code](https://github.com/iceman126/pysc2-agents/blob/master/policies.py))
  - softmax(x)
  - select_action(x, available_actions)
  - select_action_greedy(x, available_actions)
  - select_action_policy(x, available_actions)
  - select_position(x)
  - select_position_policy(x)
  - softmax_sample(x)
- Atari(object)
  - step(screen, minimap, ns, *_args, **_kwargs)
  - step_policy(screen, minimap, ns, *_args, **_kwargs)
  - step_greedy(screen, minimap, ns, epsilon, *_args, **_kwargs)
  - step_epsilon(screen, minimap, ns, *_args, **_kwargs)
  - value(screen, minimap, ns, *_args, **_kwargs)
- Atari(object)
  - step(screen, minimap, ns, *_args, **_kwargs)
  - value(screen, minimap, ns, *_args, **_kwargs)

File: iceman126/pysc2-agents/run_sc2.py ([code](https://github.com/iceman126/pysc2-agents/blob/master/run_sc2.py))
  - train(map_name, num_timesteps, batch_steps, seed, network, ar, lr, lrschedule, screen_size, minimap_size, step_mul, num_cpu, optimizer, ent_coef, vl_coef, max_grad_norm)
  - make_env(rank)
  - main(unused_argv)

File: iceman126/pysc2-agents/utils.py ([code](https://github.com/iceman126/pysc2-agents/blob/master/utils.py))
  - process_catagorical_map(input_map, scale)
  - process_state(state)
  - copy_graph(to_scope, from_scope)
  - mse(pred, target)

## inoryy-pysc2-rl-agent

**Repo: inoryy/pysc2-rl-agent** ([repo link](https://github.com/inoryy/pysc2-rl-agent))

File: inoryy/pysc2-rl-agent/common/config.py ([code](https://github.com/inoryy/pysc2-rl-agent/blob/master/common/config.py))
- Config
  - build(self, cfg_path)
  - map_id(self)
  - full_id(self)
  - policy_dims(self)
  - screen_dims(self)
  - minimap_dims(self)
  - non_spatial_dims(self)
  - preprocess(self, obs)
  - save(self, cfg_path)
  - is_spatial(arg)

File: inoryy/pysc2-rl-agent/common/env.py ([code](https://github.com/inoryy/pysc2-rl-agent/blob/master/common/env.py))
  - make_envs(args)
  - make_env(sz=32, **params)
  - worker(remote, env_fn_wrapper)
- CloudpickleWrapper(object)
- EnvPool(object)
  - spec(self)
  - step(self, actions)
  - reset(self)
  - close(self)
  - save_replay(self, replay_dir='PySC2Replays')
  - num_envs(self)

File: inoryy/pysc2-rl-agent/common/__init__.py ([code](https://github.com/inoryy/pysc2-rl-agent/blob/master/common/__init__.py))
  - flatten(x)
  - flatten_dicts(x)
  - flatten_lists(x)

File: inoryy/pysc2-rl-agent/rl/agent.py ([code](https://github.com/inoryy/pysc2-rl-agent/blob/master/rl/agent.py))
- A2CAgent
  - train(self, step, states, actions, rewards, dones, last_value, ep_rews)
  - act(self, state)
  - get_value(self, state)
  - select(acts, policy)
  - sample(probs)
  - clip_log(probs)
  - summarize(**kwargs)

File: inoryy/pysc2-rl-agent/rl/env_wrapper.py ([code](https://github.com/inoryy/pysc2-rl-agent/blob/master/rl/env_wrapper.py))
- EnvWrapper
  - step(self, acts)
  - reset(self)
  - wrap_actions(self, actions)
  - wrap_results(self, results)
  - save_replay(self, replay_dir='PySC2Replays')
  - spec(self)
  - close(self)
  - num_envs(self)

File: inoryy/pysc2-rl-agent/rl/model.py ([code](https://github.com/inoryy/pysc2-rl-agent/blob/master/rl/model.py))
  - fully_conv(config)
  - cnn_block(sz, dims, embed_dim_fn)
  - non_spatial_block(sz, dims, idx)
  - broadcast(tensor, sz)
  - mask_probs(probs, mask)

File: inoryy/pysc2-rl-agent/rl/runner.py ([code](https://github.com/inoryy/pysc2-rl-agent/blob/master/rl/runner.py))
- Runner
  - run(self, num_updates=1, train=True)
  - collect_rollout(self)
  - reset(self)
  - log(self, rewards, dones)

## chris-chris-pysc2-examples

**Repo: chris-chris/pysc2-examples** ([repo link](https://github.com/chris-chris/pysc2-examples))

File: chris-chris/pysc2-examples/a2c/a2c.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/a2c/a2c.py))
- Model(object)
  - train(obs, states, td_targets, masks, actions, xy0, xy1, values)
  - save(save_path)
  - load(load_path)
- Runner(object)
  - update_obs(self, obs)
  - update_available(self, _available_actions)
  - valid_base_action(self, base_actions)
  - trans_base_actions(self, base_actions)
  - construct_action(self, base_actions, base_action_spec, x0, y0, x1, y1)
  - run(self)
  - learn(policy,

File: chris-chris/pysc2-examples/a2c/kfac.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/a2c/kfac.py))
- KfacOptimizer()
  - getFactors(self, g, varlist)
  - searchFactors(gradient, graph)
  - getStats(self, factors, varlist)
  - compute_and_apply_stats(self, loss_sampled, var_list=None)
  - compute_stats(self, loss_sampled, var_list=None)
  - apply_stats(self, statsUpdates)
  - updateAccumStats()
  - updateRunningAvgStats(statsUpdates, fac_iter=1)
  - dequeue_stats_op()
  - getStatsEigen(self, stats=None)
  - computeStatsEigen(self)
  - removeNone(tensor_list)
  - copyStats(var_list)
  - applyStatsEigen(self, eigen_list)
  - getKfacPrecondUpdates(self, gradlist, varlist)
  - compute_gradients(self, loss, var_list=None)
  - apply_gradients_kfac(self, grads)
  - dequeue_op()
  - no_op_wrapper()
  - gradOp()
  - getKfacGradOp()
  - optimOp()
  - updateOptimOp()
  - apply_gradients(self, grads)
  - coldSGDstart()
  - warmKFACstart()
  - minimize(self, loss, loss_sampled, var_list=None)

File: chris-chris/pysc2-examples/a2c/policies.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/a2c/policies.py))
- CnnPolicy(object)
  - step(ob, *_args, **_kwargs)
  - value(ob, *_args, **_kwargs)
  - act(self, ob)

File: chris-chris/pysc2-examples/common/common.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/common/common.py))
  - init(env, obs)
  - solve_tsp(
  - report_sol(obj, s="")
  - group_init_queue(player_relative)
  - update_group_list2(control_group)
  - check_group_list2(extra)
  - update_group_list(obs)
  - check_group_list(env, obs)
  - shift(direction, number, matrix)
  - select_marine(env, obs)
  - marine_action(env, obs, player, action)

File: chris-chris/pysc2-examples/common/core.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/common/core.py))
- Space(object)
  - sample(self)
  - contains(self, x)
  - to_jsonable(self, sample_n)
  - from_jsonable(self, sample_n)

File: chris-chris/pysc2-examples/common/spaces/box.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/common/spaces/box.py))
- Box(core.Space)
  - sample(self)
  - contains(self, x)
  - to_jsonable(self, sample_n)
  - from_jsonable(self, sample_n)
  - shape(self)

File: chris-chris/pysc2-examples/common/spaces/discrete.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/common/spaces/discrete.py))
- Discrete(Space)
  - sample(self)
  - contains(self, x)
  - shape(self)

File: chris-chris/pysc2-examples/common/spaces/multi_discrete.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/common/spaces/multi_discrete.py))
- MultiDiscrete(core.Space)
  - sample(self)
  - contains(self, x)
  - shape(self)

File: chris-chris/pysc2-examples/common/spaces/prng.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/common/spaces/prng.py))
  - seed(seed=None)

File: chris-chris/pysc2-examples/common/spaces/tuple_space.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/common/spaces/tuple_space.py))
- Tuple(Space)
  - sample(self)
  - contains(self, x)
  - to_jsonable(self, sample_n)
  - from_jsonable(self, sample_n)

File: chris-chris/pysc2-examples/common/vec_env/__init__.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/common/vec_env/__init__.py))
- VecEnv(object)
  - step(self, vac)
  - reset(self)
  - close(self)

File: chris-chris/pysc2-examples/common/vec_env/subproc_vec_env.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/common/vec_env/subproc_vec_env.py))
  - worker(remote, map_name, nscripts, i)
- SubprocVecEnv(VecEnv)
  - step(self, actions)
  - reset(self)
  - action_spec(self, base_actions)
  - close(self)
  - step_async(self, actions)
  - step_wait(self)
  - num_envs(self)

File: chris-chris/pysc2-examples/deepq_mineral_4way.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/deepq_mineral_4way.py))
- ActWrapper(object)
  - load(path, act_params, num_cpu=16)
  - save(self, path)
  - load(path, act_params, num_cpu=16)
  - learn(env,
  - make_obs_ph(name)
  - intToCoordinate(num, size=64)
  - shift(direction, number, matrix)

File: chris-chris/pysc2-examples/deepq_mineral_shards.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/deepq_mineral_shards.py))
- ActWrapper(object)
  - load(path, act_params, num_cpu=16)
  - save(self, path)
  - load(path, act_params, num_cpu=16)
  - learn(env,
  - make_obs_ph(name)
  - intToCoordinate(num, size=32)
  - shift(direction, number, matrix)

File: chris-chris/pysc2-examples/defeat_zerglings/demo_agent.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/defeat_zerglings/demo_agent.py))
- MarineAgent(base_agent.BaseAgent)
  - step(self, obs)

File: chris-chris/pysc2-examples/defeat_zerglings/dqfd.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/defeat_zerglings/dqfd.py))
- ActWrapper(object)
  - load(path, act_params, num_cpu=16)
  - save(self, path)
  - load(path, act_params, num_cpu=16)
  - learn(env,
  - make_obs_ph(name)

File: chris-chris/pysc2-examples/defeat_zerglings/noop_agent.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/defeat_zerglings/noop_agent.py))
- NOOPAgent(base_agent.BaseAgent)
  - step(self, obs)

File: chris-chris/pysc2-examples/defeat_zerglings/run_demo_agent.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/defeat_zerglings/run_demo_agent.py))
  - main()

File: chris-chris/pysc2-examples/demo.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/demo.py))
  - construct_action(marine_num, x, y)
  - get_position(env, marine_num)
  - main()

File: chris-chris/pysc2-examples/enjoy_mineral_shards.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/enjoy_mineral_shards.py))
  - main()
  - make_obs_ph(name)
  - shift(direction, number, matrix)

File: chris-chris/pysc2-examples/main.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/main.py))
  - main()
  - deepq_callback(locals, globals)
  - deepq_4way_callback(locals, globals)
  - a2c_callback(locals, globals)

File: chris-chris/pysc2-examples/maps/chris_maps.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/maps/chris_maps.py))
- ChrisMaps(lib.Map)

File: chris-chris/pysc2-examples/mineral/run_scripted_agent.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/mineral/run_scripted_agent.py))
  - main()

File: chris-chris/pysc2-examples/mineral/scripted_agent.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/mineral/scripted_agent.py))
- CollectMineralShards(base_agent.BaseAgent)
  - step(self, obs)
  - report_sol(obj, s="")
- CollectMineralShards2(base_agent.BaseAgent)
  - step(self, obs)

File: chris-chris/pysc2-examples/mineral/tsp2.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/mineral/tsp2.py))
  - distL2(x1,y1, x2,y2)
  - distL1(x1,y1, x2,y2)
  - mk_matrix(coord, dist)
  - read_tsplib(filename)
  - mk_closest(D, n)
  - length(tour, D)
  - randtour(n)
  - nearest(last, unvisited, D)
  - nearest_neighbor(n, i, D)
  - exchange_cost(tour, i, j, D)
  - exchange(tour, tinv, i, j)
  - improve(tour, z, D, C)
  - localsearch(tour, z, D, C=None)
  - multistart_localsearch(k, n, D, report=None)
  - report_sol(obj, s="")

File: chris-chris/pysc2-examples/mineral/tsp.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/mineral/tsp.py))
  - distance(point1, point2)
  - total_distance(points)
  - travelling_salesman(points, start=None)
  - optimized_travelling_salesman(points, start=None)
  - main()

File: chris-chris/pysc2-examples/nsml.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/nsml.py))
  - report(*args, **kwargs)
  - bind(*args, **kwargs)
  - save(*args, **kwargs)

File: chris-chris/pysc2-examples/replay_mineral.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/replay_mineral.py))
  - sorted_dict_str(d)
- ReplayStats(object)
  - merge(self, other)
  - merge_dict(a, b)
- ProcessStats(object)
  - update(self, stage)
  - valid_replay(info, ping)
- ReplayProcessor(multiprocessing.Process)
  - run(self)
  - process_replay(self, controller, replay_data, map_data, player_id)
  - stats_printer(stats_queue)
  - replay_queue_filler(replay_queue, replay_list)
  - main(unused_argv)

File: chris-chris/pysc2-examples/tests/scripted_test.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/tests/scripted_test.py))
- TestScripted(utils.TestCase)
  - test_defeat_zerglings(self)

File: chris-chris/pysc2-examples/train_defeat_zerglings.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/train_defeat_zerglings.py))
  - main()
  - deepq_callback(locals, globals)
  - acktr_callback(locals, globals)

File: chris-chris/pysc2-examples/train_mineral_shards.py ([code](https://github.com/chris-chris/pysc2-examples/blob/master/train_mineral_shards.py))
  - main()
  - deepq_callback(locals, globals)
  - deepq_4way_callback(locals, globals)
  - a2c_callback(locals, globals)

## simonmeister-pysc2-rl-agents

**Repo: simonmeister/pysc2-rl-agents** ([repo link](https://github.com/simonmeister/pysc2-rl-agents))

File: simonmeister/pysc2-rl-agents/rl/agents/a2c/agent.py ([code](https://github.com/simonmeister/pysc2-rl-agents/blob/master/rl/agents/a2c/agent.py))
- A2CAgent()
  - build(self, static_shape_channels, resolution, scope=None, reuse=None)
  - get_obs_feed(self, obs)
  - get_actions_feed(self, actions)
  - train(self, obs, actions, returns, advs, summary=False)
  - step(self, obs)
  - get_value(self, obs)
  - init(self)
  - save(self, path, step=None)
  - load(self, path)
  - mask_unavailable_actions(available_actions, fn_pi)
  - compute_policy_entropy(available_actions, policy, actions)
  - compute_entropy(probs)
  - sample_actions(available_actions, policy)
  - sample(probs)
  - compute_policy_log_probs(available_actions, policy, actions)
  - compute_log_probs(probs, labels)

File: simonmeister/pysc2-rl-agents/rl/agents/a2c/agent_test.py ([code](https://github.com/simonmeister/pysc2-rl-agents/blob/master/rl/agents/a2c/agent_test.py))
- A2CAgentTest(tf.test.TestCase)
  - test_compute_policy_log_probs(self)
  - test_compute_policy_entropy(self)

File: simonmeister/pysc2-rl-agents/rl/agents/a2c/runner.py ([code](https://github.com/simonmeister/pysc2-rl-agents/blob/master/rl/agents/a2c/runner.py))
- A2CRunner()
  - reset(self)
  - get_mean_score(self)
  - run_batch(self, train_summary=False)
  - compute_returns_advantages(rewards, dones, values, next_values, discount)
  - actions_to_pysc2(actions, size)
  - mask_unused_argument_samples(actions)
  - flatten_first_dims(x)
  - flatten_first_dims_dict(x)
  - stack_and_flatten_actions(lst, axis=0)

File: simonmeister/pysc2-rl-agents/rl/agents/a2c/runner_test.py ([code](https://github.com/simonmeister/pysc2-rl-agents/blob/master/rl/agents/a2c/runner_test.py))
- A2CRunnerTest(tf.test.TestCase)

File: simonmeister/pysc2-rl-agents/rl/environment.py ([code](https://github.com/simonmeister/pysc2-rl-agents/blob/master/rl/environment.py))
- SingleEnv
  - step(self, actions)
  - reset(self)
  - close(self)
  - observation_spec(self)
  - worker(remote, env_fn_wrapper)
- CloudpickleWrapper(object)
- SubprocVecEnv
  - step(self, actions)
  - reset(self)
  - close(self)
  - observation_spec(self)
  - make_sc2env(**kwargs)

File: simonmeister/pysc2-rl-agents/rl/networks/fully_conv.py ([code](https://github.com/simonmeister/pysc2-rl-agents/blob/master/rl/networks/fully_conv.py))
- FullyConv()
  - embed_obs(self, x, spec, embed_fn)
  - log_transform(self, x, scale)
  - embed_spatial(self, x, dims)
  - embed_flat(self, x, dims)
  - input_conv(self, x, name)
  - non_spatial_output(self, x, channels)
  - spatial_output(self, x)
  - concat2d(self, lst)
  - broadcast_along_channels(self, flat, size2d)
  - to_nhwc(self, map2d)
  - from_nhwc(self, map2d)
  - build(self, screen_input, minimap_input, flat_input)

File: simonmeister/pysc2-rl-agents/rl/networks/fully_conv_test.py ([code](https://github.com/simonmeister/pysc2-rl-agents/blob/master/rl/networks/fully_conv_test.py))
- FullyConvTest(tf.test.TestCase)
  - test_embed_obs(self)

File: simonmeister/pysc2-rl-agents/rl/pre_processing.py ([code](https://github.com/simonmeister/pysc2-rl-agents/blob/master/rl/pre_processing.py))
  - stack_ndarray_dicts(lst, axis=0)
- Preprocessor()
  - get_input_channels(self)
  - preprocess_obs(self, obs_list)

File: simonmeister/pysc2-rl-agents/rl/util.py ([code](https://github.com/simonmeister/pysc2-rl-agents/blob/master/rl/util.py))
  - safe_div(numerator, denominator, name="value")
  - safe_log(x)
  - main()

## pekaalto-sc2aibot

**Repo: pekaalto/sc2aibot** ([repo link](https://github.com/pekaalto/sc2aibot))

File: pekaalto/sc2aibot/actorcritic/policy.py ([code](https://github.com/pekaalto/sc2aibot/blob/master/actorcritic/policy.py))
- FullyConvPolicy
  - build(self)
  - logclip(x)

File: pekaalto/sc2aibot/actorcritic/runner.py ([code](https://github.com/pekaalto/sc2aibot/blob/master/actorcritic/runner.py))
- Runner(object)
  - reset(self)
  - run_batch(self)

File: pekaalto/sc2aibot/common/multienv.py ([code](https://github.com/pekaalto/sc2aibot/blob/master/common/multienv.py))
- SingleEnv
  - step(self, actions)
  - reset_done_envs(self)
  - reset(self)
  - close(self)
  - worker(remote, env_fn_wrapper)
- CloudpickleWrapper(object)
- SubprocVecEnv
  - step(self, actions)
  - reset(self)
  - close(self)
  - reset_done_envs(self)
  - make_sc2env(**kwargs)

File: pekaalto/sc2aibot/common/preprocess.py ([code](https://github.com/pekaalto/sc2aibot/blob/master/common/preprocess.py))
  - log_transform(x, scale)
  - get_visibility_flag(visibility_feature)
  - numeric_idx_and_scale(set)
  - stack_list_of_dicts(d)
  - get_available_actions_flags(obs)
- ObsProcesser
  - get_screen_numeric(self, obs)
  - get_mimimap_numeric(self, obs)
  - process_one_input(self, timestep
  - process(self, obs_list)
  - combine_batch(self, mb_obs)
  - make_default_args(arg_names)
  - convert_point_to_rectangle(point, delta, dim)
  - l(x)
  - arg_names()
  - find_rect_function_id()
- ActionProcesser
  - make_one_action(self, action_id, spatial_coordinates)
  - process(self, action_ids, spatial_action_2ds)
  - combine_batch(self, mb_actions)

File: pekaalto/sc2aibot/common/test_processers.py ([code](https://github.com/pekaalto/sc2aibot/blob/master/common/test_processers.py))
- TestActionProcesser(TestCase)
  - test_simple(self)
  - test_rectangle(self)
  - test_invalid_and_dim(self)
- TestObsProcesser
  - test_one_input(self)

File: pekaalto/sc2aibot/common/test_util.py ([code](https://github.com/pekaalto/sc2aibot/blob/master/common/test_util.py))
- TestUtil(tf.test.TestCase)
  - test_dict_list_transpose(self)
  - test_weighted_random_sample(self)
  - test_select_from_each_row(self)
  - test_calculate_n_step_reward(self)
  - discount_with_dones(rewards, dones, gamma)
  - test_calculate_n_step_reward1(self)
  - test_combine_first_dimensions(self)
  - test_ravel_index_pairs(self)
- TestGeneralNStepAdvantage(tf.test.TestCase)
  - test_gamma_one(self)
  - test_gamma_zero(self)
  - test_general(self)
  - test_general_batch(self)
  - test_general_n_step_advantage_3(self)

File: pekaalto/sc2aibot/common/util.py ([code](https://github.com/pekaalto/sc2aibot/blob/master/common/util.py))
  - weighted_random_sample(weights)
  - select_from_each_row(params, indices)
  - calculate_n_step_reward(
  - general_n_step_advantage(
  - combine_first_dimensions(x
  - ravel_index_pairs(idx_pairs, n_col)
  - dict_of_lists_to_list_of_dicst(x

File: pekaalto/sc2aibot/run_agent.py ([code](https://github.com/pekaalto/sc2aibot/blob/master/run_agent.py))
  - check_and_handle_existing_folder(f)
  - main()
