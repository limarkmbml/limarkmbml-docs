# Overview
This list shows examples of pysc2 agents.

The listings follow this convention:
> **Repository name (link to repo)**
>
> Filename (link to code)
> - Class
>   - Method

# Contents
1. [xhujoy-pysc2-agents](#xhujoy-pysc2-agents)
2. [skjb-pysc2-tutorial](#skjb-pysc2-tutorial)

# List of examples


## xhujoy-pysc2-agents

**Repo: xhujoy/pysc2-agents** ([repo link](https://github.com/xhujoy/pysc2-agents))

File: xhujoy/pysc2-agents/agents/a3c_agent.py ([code](https://github.com/xhujoy/pysc2-agents/blob/master/agents/a3c_agent.py))
- A3CAgent(object)
  - setup(self, sess, summary_writer)
  - initialize(self)
  - reset(self)
  - build_model(self, reuse, dev, ntype)
  - step(self, obs)
  - update(self, rbs, disc, lr, cter)
  - save_model(self, path, count)
  - load_model(self, path)

File: xhujoy/pysc2-agents/agents/network.py ([code](https://github.com/xhujoy/pysc2-agents/blob/master/agents/network.py))
  - build_net(minimap, screen, info, msize, ssize, num_action, ntype)
  - build_atari(minimap, screen, info, msize, ssize, num_action)
  - build_fcn(minimap, screen, info, msize, ssize, num_action)

File: xhujoy/pysc2-agents/main.py ([code](https://github.com/xhujoy/pysc2-agents/blob/master/main.py))
  - run_thread(agent, map_name, visualize)

File: xhujoy/pysc2-agents/run_loop.py ([code](https://github.com/xhujoy/pysc2-agents/blob/master/run_loop.py))
  - run_loop(agents, env, max_frames=0)

File: xhujoy/pysc2-agents/utils.py ([code](https://github.com/xhujoy/pysc2-agents/blob/master/utils.py))
  - preprocess_minimap(minimap)
  - preprocess_screen(screen)
  - minimap_channel()
  - screen_channel()


## skjb-pysc2-tutorial

**Repo: skjb/pysc2-tutorial** ([repo link](https://github.com/skjb/pysc2-tutorial))

File: skjb/pysc2-tutorial/BuildaZergBot/zerg_agent_step7.py ([code](https://github.com/skjb/pysc2-tutorial/blob/master/BuildaZergBot/zerg_agent_step7.py))
- ZergAgent(base_agent.BaseAgent)
  - unit_type_is_selected(self, obs, unit_type)
  - get_units_by_type(self, obs, unit_type)
  - can_do(self, obs, action)
  - step(self, obs)
  - main(unused_argv)

File: skjb/pysc2-tutorial/BuildingaBasicAgent/simple_agent_step4.py ([code](https://github.com/skjb/pysc2-tutorial/blob/master/BuildingaBasicAgent/simple_agent_step4.py))
- SimpleAgent(base_agent.BaseAgent)
  - transformLocation(self, x, x_distance, y, y_distance)
  - step(self, obs)

File: skjb/pysc2-tutorial/BuildinganAttackAgent/attack_agent_step6.py ([code](https://github.com/skjb/pysc2-tutorial/blob/master/BuildinganAttackAgent/attack_agent_step6.py))
- QLearningTable
  - choose_action(self, observation)
  - learn(self, s, a, r, s_)
  - check_state_exist(self, state)
- AttackAgent(base_agent.BaseAgent)
  - transformDistance(self, x, x_distance, y, y_distance)
  - transformLocation(self, x, y)
  - step(self, obs)

File: skjb/pysc2-tutorial/BuildingaSmartAgent/smart_agent_step5.py ([code](https://github.com/skjb/pysc2-tutorial/blob/master/BuildingaSmartAgent/smart_agent_step5.py))
- QLearningTable
  - choose_action(self, observation)
  - learn(self, s, a, r, s_)
  - check_state_exist(self, state)
- SmartAgent(base_agent.BaseAgent)
  - transformLocation(self, x, x_distance, y, y_distance)
  - step(self, obs)

File: skjb/pysc2-tutorial/BuildingaSparseRewardAgent/sparse_agent_step7.py ([code](https://github.com/skjb/pysc2-tutorial/blob/master/BuildingaSparseRewardAgent/sparse_agent_step7.py))
- QLearningTable
  - choose_action(self, observation)
  - learn(self, s, a, r, s_)
  - check_state_exist(self, state)
- SparseAgent(base_agent.BaseAgent)
  - transformDistance(self, x, x_distance, y, y_distance)
  - transformLocation(self, x, y)
  - splitAction(self, action_id)
  - step(self, obs)

File: skjb/pysc2-tutorial/RefiningtheSparseRewardAgent/refined_agent.py ([code](https://github.com/skjb/pysc2-tutorial/blob/master/RefiningtheSparseRewardAgent/refined_agent.py))
- QLearningTable
  - choose_action(self, observation, excluded_actions=[])
  - learn(self, s, a, r, s_)
  - check_state_exist(self, state)
- SparseAgent(base_agent.BaseAgent)
  - transformDistance(self, x, x_distance, y, y_distance)
  - transformLocation(self, x, y)
  - splitAction(self, action_id)
  - step(self, obs)
- ACMode
- ActorCriticAgent
  - init(self)
  - build_model(self)
  - step(self, obs)
  - train(self, input_dict)
  - get_value(self, obs)
  - flush_summaries(self)
  - save(self, path, step=None)
  - load(self, path)
  - update_theta(self)

